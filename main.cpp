// Example program
#include <iostream>
#include <string>
#include "typelist.hpp"


int main()
{
    typedef TypeList<int, float, int> list1;
    typedef TypeList<char, list1, double, list1> list2;
    //testype pouet;
    std::cout << list1::size << std::endl;
    std::cout << list2::size << std::endl;
    std::cout << list2::at<char>::index << std::endl;
    std::cout << std::is_same<list1::at<void, 1>::type, int>::value << std::endl;
}

