#pragma once

#include <type_traits>


template <class... types>
struct TypeList;

template <>
struct TypeList<> {
    unsigned static const size = 0;

private:
    typedef TypeList<> self;

    template <class... args>
    friend class TypeList;

    // contains
    template <class T, unsigned idx = 0, class dummy=void>
    struct _contains {
        static const bool value = false;
    };

    // getType
    template <class T, unsigned idx = 0, class = void>
    struct _getType {
      static_assert(self::_contains<T, idx>:: value, "Type not Found");
      typedef struct {} type;
    };

    template <unsigned idx, class dummy>
    struct _getType<void, idx, dummy> {
      static_assert(idx < self::size, "Out of bounds");
      typedef struct {} type;
    };

    // indexOf
    template <class T, unsigned idx = 0, class = void>
    struct _indexOf {
      static_assert(self::_contains<T, idx>:: value, "Type not Found");
      static const unsigned value = -1;
    };

    template <unsigned idx, class dummy>
    struct _indexOf<void, idx, dummy> {
      static_assert(idx < self::size, "Out of bounds");
      static const unsigned value = -1;
    };

    // at helper
    template <class T, unsigned idx = 0, class = void>
    struct _at {
      static const unsigned index = _indexOf<T, idx>::value;
      typedef typename _getType<T, idx>::type type;
    };

    template <class T, unsigned idx>
    struct _at<T, idx, typename std::enable_if<!self::template _contains<T, idx>::value>::type> {
      static_assert(idx < self::size, "Type not found");
      static const unsigned index = -1;
      typedef struct {} type;
    };

    template <unsigned idx>
    struct _at<void, idx, typename std::enable_if<!self::template _contains<void, idx>::value>::type> {
      static_assert(idx < self::size, "Out of bounds");
      static const unsigned index = -1;
      typedef struct {} type;
    };
public:
    template <class T, unsigned idx = 0>
    struct contains {
      static const bool value = self::template _contains<T, idx>::value;
    };

    template <class T, unsigned idx = 0>
    struct at {
      static const unsigned index = _at<T, idx>::index;
      typedef typename _at<T, idx>::type type;
    };
};


template <class head, class... tail>
struct TypeList<head, tail...> {
    typedef TypeList<head, tail...> self;
    typedef head head_type;
    typedef TypeList<tail...> tail_list;

    unsigned static const size = 1 + tail_list::size;
    static_assert(!std::is_same<head, void>::value, "TypeList cannot handle void type");

private:
    template <class... args>
    friend class TypeList;


    // Contains
    template <class T, unsigned idx = 0, class = void>
    struct _contains {
      static const bool value = tail_list::template _contains<T, idx>::value;
    };

    template <unsigned idx, class dummy>
    struct _contains<head, idx, dummy> {
      static const bool value = tail_list::template _contains<head, idx-1>::value;
    };

    template <class dummy>
    struct _contains<head, 0, dummy> {
      static const bool value = true;
    };

    template <unsigned idx, class dummy>
    struct _contains<void, idx, dummy> {
      static const bool value = self::size > idx;
    };


    // getType
    template <class T, unsigned idx, class = void>
    struct _getType {
      static_assert(self::_contains<T, idx>::value, "Type not Found");
      typedef T type;
    };

    template <unsigned idx, class dummy>
    struct _getType<typename std::enable_if<(idx != 0 && idx < self::size), void>::type, idx, dummy> {
      typedef typename tail_list::template _getType<void, idx-1>::type type;
    };

    template <unsigned idx, class dummy>
    struct _getType<typename std::enable_if<(idx != 0 && idx >= self::size), void>::type, idx, dummy> {
      static_assert(idx < self::size, "Out of bounds");
      typedef struct {} type;
    };

    template <unsigned idx, class dummy>
    struct _getType<typename std::enable_if<idx == 0, void>::type, idx, dummy> {
      typedef head_type type;
    };


    // indexOf
    template <class T, unsigned idx, class = void>
    struct _indexOf {
      static const unsigned value = 1 + tail_list::template _indexOf<T, idx>::value;
    };

    template <class T, unsigned idx>
    struct _indexOf<T, idx, typename std::enable_if<!self::_contains<T, idx>::value && !std::is_same<T, void>::value>::type> {
      static_assert(self::_contains<T, idx>::value, "Type not found");
      static const unsigned value = -1;
    };

    template <unsigned idx, class dummy>
    struct _indexOf<typename std::enable_if<self::_contains<head_type, idx>::value, head_type>::type, idx, dummy> {
      static const unsigned value = 1 + tail_list::template _indexOf<head_type, idx-1>::value;
    };

    template <class dummy>
    struct _indexOf<typename std::enable_if<self::_contains<head_type, 0>::value, head_type>::type, 0, dummy> {
      static const unsigned value = 0;
    };

    template <unsigned idx, class dummy>
    struct _indexOf<void, idx, dummy> {
      static_assert(idx < self::size, "Out of bounds");
      static const unsigned value = idx;
    };

    // at helper
    template <class T, unsigned idx = 0, class = void>
    struct _at {
      static const unsigned index = _indexOf<T, idx>::value;
      typedef typename _getType<T, idx>::type type;
    };

    template <class T, unsigned idx>
    struct _at<T, idx, typename std::enable_if<!self::template _contains<T, idx>::value>::type> {
      static_assert(idx < self::size, "Type not found");
      static const unsigned index = -1;
      typedef struct {} type;
    };

    template <unsigned idx>
    struct _at<void, idx, typename std::enable_if<!self::template _contains<void, idx>::value>::type> {
      static_assert(idx < self::size, "Out of bounds");
      static const unsigned index = -1;
      typedef struct {} type;
    };
public:
    template <class T, unsigned idx = 0>
    struct contains {
      static const bool value = self::template _contains<T, idx>::value;
    };

    template <class T, unsigned idx = 0>
    struct at {
      static const unsigned index = _at<T, idx>::index;
      typedef typename _at<T, idx>::type type;
    };
};
